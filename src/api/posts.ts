import api from  './index'
import { UserType } from '@/components/molecules/CreatePost/CreatePost';

export const PostsApi = {
    getAll: () => api.get('/posts'),
    getById: (postId: number) => api.get('/posts', {params: {postId}}),
    updatePost: (postId: number) => api.put(`/posts/${postId}`),
    deletePost: (postId: number) => api.delete(`/posts/${postId}`),
    addPost: (newPost: string,user: UserType) => api.post('/posts', {params: { newPost, user }})
};
export const PostCommentsApi = {
    getAll: (postId: number) => api.get(`/posts/${postId}/comments`)
}