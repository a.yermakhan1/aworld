import api from  './index'

export const UsersApi = {
    getAll: () => api.get('/users'),
    getById: (userId:number) => api.get('/posts', {params: {userId}}),
};
export const UserLoginApi = {
    postLogin: (email: string, password: string) => api.post('/auth/login', {email, password})
}