import React, { FC, useContext } from 'react';
import UsersContext from '../../../context/UsersProvider';
import './Tab.css'
import RecUserBlock from '../../molecules/RecUserBlock/RecUserBlock';

const Tab: FC = () => {

  const usersData = useContext(UsersContext);
  const usersFollow = usersData.slice(0,3);

  return (
    <div className="right_tab_container">
      <input type="text" placeholder='Search aworld'/>  
      <div className='recomendations_container'>
        <div className='recomendations_header'>
          <h3>Who to follow</h3>
        </div>
        {usersFollow.map(user => 
          (
          <div>
            <div className='recomendations_user'>
              <RecUserBlock 
              username={user.username}
              name={user.name}
              />
            </div>
            <hr/>
          </div>          
          )
        )
        }
      </div>
    </div>
  );
}

export default Tab;