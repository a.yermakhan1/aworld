import React, { FC, useEffect, useContext } from 'react';
import './Feed.css';
import DataContext from '../../../context/DataProvider';
import UsersContext from '../../../context/UsersProvider';
import PostBlock from '../../molecules/PostBlock/PostBlock';
import Customize from '../../../svg/Customize.svg';
import CreatePost from '../../molecules/CreatePost/CreatePost';
import { UserType} from '@/components/pages/Home/Home';


const Feed: FC = () => {
  const postsData = useContext(DataContext);
  const usersData = useContext(UsersContext);

  const nullUser: UserType = {
    email: "anmanna",
    name: "anney",
    phone: "8885643543",
    username: "anmanna",
    website: "dfcsa",
    id: 11,
    userId: 11,
    company: {name: "lol"},
    address: {name: "lol"}
  };

  useEffect(() => {
    console.log("hkljljh", postsData[0])
  }, [])

  return (
    <div className="feed_container">
      <div className='feed_header'>
        <div>
          <h3>Home</h3>
        </div>
        <div><img src={Customize} alt="shiny stars"/></div>
        <hr className='right_line'/>
        <hr className='left_line'/>
      </div>
      <hr/>
      <CreatePost/>
      <hr/>
      <div className='post_element'>
      {
        postsData.map(post => 
          (
            <div>
              <PostBlock
              body = {post.body}
              id = {post.id}
              user = {usersData[post.userId] ? usersData[post.userId] : nullUser}
            />       
              <hr/>            
            </div>
          ))

        }
      </div>
    </div>
  );
}

export default Feed;