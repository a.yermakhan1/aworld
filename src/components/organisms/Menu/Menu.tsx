import React, { FC } from 'react';
import { MenuItems } from './MenuItems';
import { NavLink } from 'react-router-dom';
import logo from '../../../svg/logo.svg'
import './Menu.css'

const Menu: FC = () => {
  return (
    <div className="menu_container">
      <ul className='nav_menu'>
        <li>
          <div className='nav_links'>
            <img src={logo} alt="logo aworld text"/>
          </div>
        </li>
        {MenuItems.map((item,index) =>
          (
          <li key={index}>
              <NavLink to={item.url}>
                  <div className = {item.className} >
                    {item.title}
                  </div>
              </NavLink>
          </li>
          )
        )}
      </ul>
    </div>
  );
}

export default Menu;