export const MenuItems: Array<{title:string, url:string, className: string}> = [
    {
        title: 'Home',
        url: '/',
        className: 'nav_links',
    },
    {
        title: 'Explore',
        url: '/explore',
        className: 'nav_links'
    }, 
    {
        title: 'Notifications',
        url: '/notifications',
        className: 'nav_links',
    },    
    {
        title: 'Bookmarks',
        url: '/bookmarks',
        className: 'nav_links'
    },
    {
        title: 'Profile',
        url: '/profile',
        className: 'nav_links'
    },
]