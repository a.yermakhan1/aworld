import React, { FC } from 'react';
import './PostBlock.css'
import UserAvatar from '../../atoms/UserAvatar';
import { PostsApi } from '../../../api/posts';
import {getRandomInt} from '../../../utils/Random';
import { UserType} from '@/components/pages/Home/Home';
import Image from '../../atoms/Image/index';

  interface postInfo {
    id: number,
    body: string,
    user: UserType,
  };
  const PostDetails: FC<postInfo> = ({id, body, user}) => {
    const deletePost = async () => {
      try{
        const res = await PostsApi.deletePost(id);
        alert( 'Post deleted');
      }catch (err) {
        console.log(err)
      }
    }
    return (
      <div className='post_details'>
          <div className='post_container'>
            <div className='user_avatar'>
              <UserAvatar width={60} height={60} imgUrl={'https://random.imagecdn.app/800/' + getRandomInt(150,200)}/>
            </div>
            <div className='post_content'>
              <div className='row_post_details'>
                <div className='post_details'>
                  <div className='username'>
                    <h3>{user.name}</h3>
                  </div>
                  <div className='user_account'>
                    <h3>@{user.username}</h3>
                  </div>
                  <div className='dot'>
                    <h3>•</h3>
                  </div>
                  <div className='time_after'>
                    <h3>{user.phone.slice(6,7)}s</h3>
                  </div>
                </div>  
                <div className='menu_btn' onClick={() => {deletePost()}}>
                  <i className="fa fa-trash" aria-hidden="true"></i>
                </div>            
              </div>

              <div className='post_text'>
                <p>
                {body}
                </p>
              </div>
              <div className='post_picture'>
                <Image             
                  src={'https://random.imagecdn.app/800/' + getRandomInt(380, 450)}
                />
              </div>
            </div>
        </div>
      </div>
    );
}

export default PostDetails;