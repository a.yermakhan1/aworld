import React, { FC, useState } from 'react';
import './RecUserBlock.css'
import UserAvatar from '../../atoms/UserAvatar';
import { getRandomInt } from '../../../utils/Random'

interface UserType {
  name: string,
  username: string
};
const RecUserBlock: FC<UserType> = ({name, username}) => {
  const [follow, setFollow] = useState(true);
  
  return (
    <div className='rec_user_block'>
      <div className='user_profile'>
        <UserAvatar width={60} height={60} imgUrl={"https://random.imagecdn.app/800/" + getRandomInt(700,800)}/>
        <div className='user_contacts'>
            <div className='username'>
            <h3>{name}</h3>
            </div>
            <div className='user_account'>
            <h3>@{username}</h3>
            </div>
        </div>        
      </div>

      <div className='follow_btn'>
          <button onClick={() => setFollow(!follow)}>
              {
              follow
              ?
              "Follow"
              :
              "Following"
              }
          </button>
      </div>            
    </div>
  );
}

export default RecUserBlock;