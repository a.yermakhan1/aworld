import React, { FC, useState } from 'react';
import './CreatePost.css';
import UserAvatar from '../../atoms/UserAvatar';
import { PostsApi } from '../../../api/posts';

export interface UserType {
  email: string,
  name: string,
  phone: string,
  username: string,
  website: string,
  id: number,
  userId: number,
  company: object,
  address: object
};
const CreatePost: FC = () => {
  const blankUser: UserType = {
    email: "Sincere@april.biz",
    id: 1,
    name: "Leanne Graham",
    phone: "1-770-736-8031 x56442",
    username: "Bret",
    website: "hildegard.org",
    userId: 0,
    company: {},
    address: {}
  }
  const [newPost, setNewPost] = useState("")
  const handleSubmit = () => {
    try{
      const res = PostsApi.addPost( newPost, blankUser )
    alert(`Post '${newPost}' created`);
    setNewPost("");
    }catch( err ) {
    console.log( err );
    }
  };
  return (
    <div className="create_post_container">
      <div className='post_create'>
        <div className='ava_input_row'>
          <div className='user_avatar'>
            <UserAvatar width={60} height={60} imgUrl="https://random.imagecdn.app/800/800"/>
          </div>
          <div className='post_input'>
            <textarea 
            placeholder='New world fact'
            onChange={(e) => setNewPost(e.target.value)}
            value={newPost} 
            />
          </div>          
        </div>
        <button 
        className={newPost.length > 0 ? "active_send" : "send"}
        disabled={newPost.length > 0 ? false : true}
        onClick={() => handleSubmit()}>
          Send
        </button>
      </div>
    </div>
  );
}

export default CreatePost;