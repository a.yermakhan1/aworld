import React, { FC } from 'react';
import './UserAvatar.css'

interface ImageProperties {
  width: number,
  height: number,
  imgUrl: string
};

  const UserAvatar: FC<ImageProperties> = ({width, height, imgUrl}) => {

  return (
    <div className='user_avatar'>
      <img 
        alt="kitty"
        width={width}
        height={height}
        src={imgUrl}
      />
    </div>
  );
}

export default UserAvatar;