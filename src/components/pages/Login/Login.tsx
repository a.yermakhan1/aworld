import React, { useState } from 'react';
import { useLocation, Navigate} from "react-router-dom";
import './Login.css';
import {UserLoginApi} from '../../../api/users';
import pass from '../../../svg/pass.svg'
import uemail from '../../../svg/uemail.svg'


const Login = () => {
    const [email, setEmail] = useState('');
    const [pwd, setPwd] = useState('');
    const [success, setSuccess] = useState(false);

    const location = useLocation();

    const handleSubmit = async() => {
        try{
            const response = await UserLoginApi.postLogin(email, pwd)
            setEmail('');
            setPwd('');
            setSuccess(true);
            console.log(response)
        }catch (err) {
            console.log(err)
        }
    }

    return(
        <div className='login'>
            
            {success ? (
                <Navigate to="/" state={{ from:location }} replace/>
            ) : (
            <section>
            <h1>Login</h1>
            <h4>Sign in to your account</h4>
            <form onSubmit={handleSubmit} className="">
                <input 
                    type="text" 
                    id="username"
                    placeholder='Username'
                    autoComplete="off"
                    onChange={(e) => setEmail(e.target.value)}
                    value={email} 
                    required
                />
                <span className='uIcon'><img src={uemail} alt="icon"/></span>
                <input 
                    type="password" 
                    id="password"
                    placeholder='Password'
                    onChange={(e) => setPwd(e.target.value)}
                    value={pwd} 
                    required
                />
                <span className='pIcon'><img src={pass} alt="icon"/></span>
                <button className='login_btn' type='submit'>Login</button>
            </form>            
        </section>
            )
            }
        </div>

    );
}

export default Login;