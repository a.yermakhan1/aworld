import React, { FC, useEffect, useState } from 'react';
import DataContext from '../../../context/DataProvider';
import UserContext from '../../../context/UsersProvider';
import Menu from '../../organisms/Menu/Menu';
import Tab from '../../organisms/Tab/Tab';
import Routing from '../../../router/Routing';
import { UsersApi } from '../../../api/users';
import { PostsApi } from '../../../api/posts';
import './Home.css'

export interface PostType {
  body: string,
  title: string,
  id: number,
  userId: number,
};
export interface UserType {
  email: string,
  name: string,
  phone: string,
  username: string,
  website: string,
  id: number,
  userId: number,
  company: object,
  address: object
};
const Home: FC = () => {
  const [users, setUsers] = useState<UserType[]>([]);
  const [posts, setPosts] = useState<PostType[]>([]);

  const GetPosts = async () => {
    try{
      const res = await PostsApi.getAll()
      const data: PostType[] = res?.data
      setPosts(data);
    }catch (err) {
      console.log(err);
    }
  }
  const GetUsers = async () => {
    try{
      const res = await UsersApi.getAll();
      const data: UserType[] = res.data
      setUsers(data);
    }catch (err) {
      console.log(err)
    }
  }
  useEffect(() => {
    GetUsers();
    GetPosts();
  }, []);
  return (
    <div className="home_container">
      <DataContext.Provider value = {posts}>
        <UserContext.Provider value = {users}>
          <div className='left_menu'>
            <Menu />
          </div>
          <div className='center_feed'>
            <Routing/>
          </div>
          <div className='right_tab'>
            <Tab />
          </div>            
        </UserContext.Provider>
      </DataContext.Provider>
    </div>
  );
}

export default Home;