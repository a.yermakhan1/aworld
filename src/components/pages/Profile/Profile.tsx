import React, { FC } from 'react';
import './Profile.css'
import UserAvatar from '../../atoms/UserAvatar';
import wallpaper from '../../../svg/wallpaper.jpg'

const Profile: FC = () => {
  
  return (
    <div className='profile'>
      <div className="profile_container">
        <div className='username_aworlds'>
            <h3>anney_sunny</h3>
            <h4>32 aworlds</h4>
        </div>
        <div className='wallpaper'>
          <hr/>
          <hr/>
          <img src={wallpaper} alt="green forest"/>
        </div>
        <div className='user_ava_edit'>
          <div className='user_ava'>
            <UserAvatar width={200} height={200} imgUrl="https://random.imagecdn.app/800/800"/>
          </div>
          <div className='edit_btn'>
            <button>Edit profile</button>
          </div>
        </div>
        <div className='user_detail'>
          <div className='profile_username'>
            <h2>
              anney_sunny
            </h2>
            <h4>
              @winter_sunflower
            </h4>

          </div>
          <div className='more_info'>
            <h4 >
              Joined January 2022
            </h4>
          </div>
          <div className='more_info'>
            <div className='follow_info'>
              <h4 >
                <strong>142</strong> Following
              </h4>
            </div>
            <div className='follow_info'>
              <h4 >
                <strong>222</strong> Followers
              </h4>
            </div>

          </div>
          <hr/>
        </div>
      </div>
    </div>
  );
}

export default Profile;