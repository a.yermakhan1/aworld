import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import './PageNotFound.css'

const PageNotFound: FC = () => {
    return(
        <div className='pageNotFound'>
            <div><h1>404 Page not found</h1></div>
            <h2>The page you're looking for doesn't exist.</h2>
            <p>Check for a typo in the URL, or </p> 
            <div className='home_link'>
                <Link to="/"><p>go to the site home</p></Link>
            </div>
        </div>
    );
}
export default PageNotFound;