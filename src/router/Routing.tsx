import React, { FC } from 'react';
import { Navigate, Routes, Route } from "react-router-dom";
import Feed from '../components/organisms/Feed/Feed';
import Bookmarks from "../components/pages/Bookmarks/Bookmarks";
import Explore from "../components/pages/Explore/Explore";
import Notifications from "../components/pages/Notifications/Notifications";
import Profile from "../components/pages/Profile/Profile";
import PageNotFound from "../components/pages/PageNotFound/PageNotFound";

const Routing: FC = () => {
  return (
      <>
        <Routes>
            <Route path="/" element={<Feed/>}/>
            <Route path="/bookmarks" element={<Bookmarks/>}/>
            <Route path="/explore" element={<Explore/>}/>
            <Route path="/notifications" element={<Notifications/>}/>
            <Route path="/profile" element={<Profile/>}/>
            <Route path="/404" element={<PageNotFound/>}/>
            <Route
              path="*"
              element={<Navigate to="/404" />}
            /> 
        </Routes>
      </>
  );
}

export default Routing;