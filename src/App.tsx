import React, { FC } from 'react';
import './App.css';
import Home from './components/pages/Home/Home';

const App: FC = () => {
  return (
    <div className="App">
      <Home />
    </div>
  );
}

export default App;
