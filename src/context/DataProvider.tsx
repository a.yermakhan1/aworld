import { createContext, useState } from "react";
import { PostType } from '@/components/pages/Home/Home';


type DataContextProviderProps = {
    children: React.ReactNode
}

const DataContext = createContext<PostType[]>([]);

export const DataContextProvider = ({ children }: DataContextProviderProps) => {
    const [posts, setPosts] = useState<PostType[]>([]);
    return(
        <DataContext.Provider value={posts}>
            {children}
        </DataContext.Provider>
    )
}

export default DataContext;