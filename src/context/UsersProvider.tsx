import { createContext, useState } from "react";
import { UserType } from '@/components/pages/Home/Home';


type UsersContextProviderProps = {
    children: React.ReactNode
}

const UsersContext = createContext<UserType[]>([]);

export const UsersContextProvider = ({ children }: UsersContextProviderProps) => {
    const [users, setUsers] = useState<UserType[]>([]);
    return(
        <UsersContext.Provider value={users}>
            {children}
        </UsersContext.Provider>
    )
}

export default UsersContext;